import 'package:flutter/material.dart';

class YellowBird extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _YellowBirdState();
  }
}

class _YellowBirdState extends State<YellowBird> {
  int cnt = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text('${cnt}'),
        ElevatedButton(onPressed: addValue, child: Text("Add+"))
      ],
    );
  }

  void addValue() {
    setState(() {
      cnt = cnt + 1;
    });
  }
}
