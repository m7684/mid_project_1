import 'package:flutter/material.dart';
import 'package:mid_project_1/green_frog.dart';
import 'package:mid_project_1/menu.dart';
import 'package:mid_project_1/yellow_bird.dart';

class Home extends StatelessWidget {
  @override
  Widget build(Object context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        bottomNavigationBar: const TabBar(
          tabs: [
            Tab(
              icon: Icon(Icons.home),
              child: Text("Home"),
            ),
            Tab(
              icon: Icon(Icons.menu_rounded),
              child: Text("Menu"),
            ),
            Tab(
              icon: Icon(Icons.rss_feed_sharp),
              child: Text("Feed"),
            ),
            Tab(
              icon: Icon(Icons.settings),
              child: Text("Setting"),
            )
          ],
        ),
        body: TabBarView(children: [
          GreenFrog(),
          Menu(),
          Text("Feeds"),
          Text("Setting"),
        ]),
      ),
    );
  }
}
