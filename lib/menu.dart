import 'package:flutter/material.dart';
import 'package:mid_project_1/model/menu_item.dart';

class Menu extends StatelessWidget {
  // List<String> menuData = [
  //   "User",
  //   "Todo",
  //   "Photos",
  //   "Albums",
  //   "Comments",
  //   "Posts"
  // ];

  List<MenuItem> menuData = [
    MenuItem("User", "List all user", const Icon(Icons.person), ""),
    MenuItem("Todo", "Todo List", const Icon(Icons.task), ""),
    MenuItem("Photos", "List all my photos", const Icon(Icons.photo), ""),
    MenuItem("Albums", "List all albums", const Icon(Icons.photo_album), ""),
    MenuItem("Comments", "List all comments", const Icon(Icons.comment), ""),
    MenuItem("Posts", "List all posts", const Icon(Icons.chat), ""),
    MenuItem("Green Frog", "Green Frog Demo", const Icon(Icons.grade_outlined),
        "/greenfrog"),
    MenuItem("Yellow Bird", "Yellow Bird Demo",
        const Icon(Icons.biotech_rounded), "/yellowbird"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Menu"),
      ),
      body: ListView.builder(
        itemCount: menuData.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(menuData[index].title),
            subtitle: Text(menuData[index].subtitle),
            leading: menuData[index].icon,
            onTap: () => {
              Navigator.pushNamed(context, menuData[index].path),
            },
          );
        },
      ),
    );
  }
}
