import 'package:flutter/material.dart';

class GreenFrog extends StatelessWidget {
  int cnt = 0;

  @override
  Widget build(Object context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Green Frog"),
      ),
      body: Column(
        children: [
          Text("${cnt}"),
          ElevatedButton(
            onPressed: addValue,
            child: const Text("Add"),
          )
        ],
      ),
    );
  }

  void addValue() {
    cnt = cnt + 1;
    print(cnt);
  }
}
