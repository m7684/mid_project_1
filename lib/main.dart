import 'package:flutter/material.dart';
import 'package:mid_project_1/green_frog.dart';
import 'package:mid_project_1/home.dart';
import 'package:mid_project_1/yellow_bird.dart';
import 'login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(Object context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // useMaterial3: false,
        // primarySwatch: Colors.indigo,
        appBarTheme: AppBarTheme(backgroundColor: Colors.pink),
      ),
      // home: MyHomePage(),
      initialRoute: '/',
      routes: {
        '/': (context) => Home(),
        // another page
        '/greenfrog': (context) => GreenFrog(),
        '/yellowbird': (context) => YellowBird(),
      },
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // your widget here
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'My App',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Login(),
    );
  }
}
