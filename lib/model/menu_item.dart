import 'package:flutter/material.dart';

class MenuItem {
  final String title;
  final String subtitle;
  final Icon icon;
  final String path;

  MenuItem(this.title, this.subtitle, this.icon, this.path);
}
